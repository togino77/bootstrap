const SDK = require('@directus/sdk-js').SDK;
require('dotenv').config({ path: '../app/server/.env' });

const url = process.env.DIRECTUS_URL;
const project = process.env.DIRECTUS_PROJ || "_";
const id = process.env.DIRECTUS_ID;
const pw = process.env.DIRECTUS_SECRET;

module.exports = (async function(){
  const client = new SDK({ url, project, mode: "jwt" });
  await client.login({url, project, email: id, password: pw});
  return client;
})();