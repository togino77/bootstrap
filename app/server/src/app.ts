import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';
import * as Sentry from '@sentry/node';

import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';

import history from 'connect-history-api-fallback';

import { Application } from './declarations';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import authentication from './authentication';
// Don't remove this comment. It's needed to format import lines nicely.

Sentry.init({ dsn: process.env.SENTRY_DSN_URL });

const app: Application = express(feathers());

// Load app configuration
app.configure(configuration());
// Load from .env
import dotenv from 'dotenv';
dotenv.config();
app.set('directus_url', process.env.DIRECTUS_URL)
app.set('directus_id', process.env.DIRECTUS_ID)
app.set('directus_secret', process.env.DIRECTUS_SECRET)
app.set('directus_proj', process.env.DIRECTUS_PROJ || '_')
app.set('natadecoco_id', process.env.NATADECOCO_ID)
app.set('natadecoco_secret', process.env.NATADECOCO_SECRET)

// Enable history api fallback
app.use(history());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

export default app;
