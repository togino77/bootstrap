import { Params } from '@feathersjs/feathers';
import { AuthenticationBaseStrategy, AuthenticationResult } from '@feathersjs/authentication';
import { SDK as Hub, ServiceType } from '@natade-coco/hub-sdk';
import { SDK as Directus } from '@directus/sdk-js';

export class NatadeCOCOStrategy extends AuthenticationBaseStrategy {
  verifyConfiguration() {
    const config = this.configuration;

    ['tokenField'].forEach(prop => {
      if (typeof config[prop] !== 'string') {
        throw new Error(`'${this.name}' authentication strategy requires a '${prop}' setting`);
      }
    });
  }

  get configuration() {
    const authConfig = this.authentication?.configuration;
    const config = super.configuration || {};

    return {
      service: authConfig.service,
      entity: authConfig.entity,
      entityId: authConfig.entityId,
      errorMessage: 'Invalid token',
      tokenField: config.tokenField,
      ...config
    };
  }

  async dataStoreConfig() {
    const url = this.app?.get('directus_url');
    const project = this.app?.get('directus_proj');
    const id = this.app?.get('directus_id');
    const pw = this.app?.get('directus_secret');
    const client = new Directus({ url, project, mode: "jwt" });
    await client.login({url, project, email: id, password: pw});
    return client.config;
  }

  async verifyJWT(jwt: string) {
    const id = this.app?.get('natadecoco_id');
    const secret = this.app?.get('natadecoco_secret');
    const client = await Hub.init({ id: id, secret: secret })
    return client.VerifyJWT(jwt, ServiceType.AppHubService)
  }

  async authenticate(data: AuthenticationResult, params: Params) {
    const { tokenField } = this.configuration;
    const token = data[tokenField];
    const vefiryResult = await this.verifyJWT(token);
    const config = await this.dataStoreConfig();
    return {
      authentication: { strategy: this.name },
      user: {
        _id: vefiryResult.issuer,
      },
      app: {
        _id: this.app?.get('natadecoco_id'),
      },
      datastore: {
        url: config.url,
        project: config.project,
        mode: config.mode,
        persist: config.persist,
        tokenExpirationTime: config.tokenExpirationTime,
        token: config.token,
        localExp: config.localExp
      }
    };
  }
}