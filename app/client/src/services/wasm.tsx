import JPQR from '@natade-coco/jpqr'
import QRCode from 'qrcode'

export interface MPMOpts {
  merchantName: string
  merchantPostalCode: string
  merchantCategoryCode: string
  merchantNameEN: string
  merchantCityEN: string
  merchantAccountInformation: string
  transactionAmount: number
}

export const getJPQR = (opts: MPMOpts): Promise<string> => {
  const code = JPQR.mpmEncode({
    payloadFormatIndicator: '01',
    pointOfInitiationMethod: 'dynamic',
    merchantAccountInformation: [{
      tag: '26',
      length: String(opts.merchantAccountInformation.length),
      value: opts.merchantAccountInformation
    }],
    merchantCategoryCode: opts.merchantCategoryCode,
    transactionCurrency: '392',
    transactionAmount: String(opts.transactionAmount),
    countryCode: 'JP',
    merchantName: opts.merchantNameEN,
    merchantCity: opts.merchantCityEN,
    postalCode: opts.merchantPostalCode,
    merchantInformation: {
      languagePreference: 'JA',
      name: opts.merchantName,
      valid: true
    }
  });
  
  return QRCode.toDataURL(code)
}