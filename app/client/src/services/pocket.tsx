import Pocket from '@natade-coco/pocket-sdk'
import { Profile } from '@natade-coco/pocket-sdk/dist/profile'
import { CCInfo } from '@natade-coco/pocket-sdk/dist/cc-info'
import QRCode from 'qrcode'

export { Profile, CCInfo }

export const getProfile = (scope: string[]) => {
  if (process.env.NODE_ENV === 'development') {
    return Promise.resolve({
      name: 'ナタデココ',
      fullName: 'ナタデココ タロウ',
      email: 'natadecoco@natade-coco.com',
      tel: '012-3456-9780',
      image: 'https://bulma.io/images/placeholders/128x128.png'
    })
  }
  return Pocket.getProfile(scope)
}

export const getCCInfo = () => {
  if (process.env.NODE_ENV === 'development') {
    return Promise.resolve({
      number: '4242424242424242',
      exp: '1225'
    })
  }
  return Pocket.getCCInfo()
}

export const getEthereumAddress = () => {
  if (process.env.NODE_ENV === 'development') {
    return Promise.resolve('0x1a2b3c')
  }
  return Pocket.getEthereumAddress()
}

export const requestSignJWT = () => {
  if (process.env.NODE_ENV === 'development') {
    return Promise.resolve(process.env.GATSBY_APP_TOKEN)
  }
  return Pocket.requestSignJWT()
}

export const connectToPayoffApp = async (target: string, path: string, allows: string[]) => {
  const url = await Pocket.connectToPayoffApp(target, path, allows)
  return QRCode.toDataURL(url)
}