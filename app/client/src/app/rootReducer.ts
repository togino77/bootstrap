import { combineReducers } from '@reduxjs/toolkit'
import exReducer from '../features/ex/slice'

const rootReducer = combineReducers({
  ex: exReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer