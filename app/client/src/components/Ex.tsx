import * as React from 'react'

const Ex = (props: any) => (
  <label className="label">
    <div className="tags has-addons">
      <span className="tag is-dark">{props.category}</span>
      <span className="tag is-light">{props.name}</span>
    </div>
  </label>
)

export default Ex