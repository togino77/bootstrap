import * as React from 'react'
import { Router, Location } from "@reach/router"

const DefaultRouter = (props: any) => (
  <Location>
    {({ location }) => (
      <Router location={location}>
        {props.children}
      </Router>
    )}
  </Location>
)

export default DefaultRouter