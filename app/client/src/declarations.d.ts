declare const graphql: (query: TemplateStringsArray) => void
declare const wasm: any
declare module "*.svg" {
  const content: string;
  export default content;
}