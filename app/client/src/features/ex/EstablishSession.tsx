import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { contextChanged, getSession } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'
import { Link } from '@reach/router'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <AboutSession />
      </div>
      <div className="column is-full">
        <GetSession />
      </div>
    </div>
  </div>
)

export default Feature

const AboutSession = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>1. セッション</strong>
    </h1>
    <p>
      セッションを確立するには、<Keyword>オフチェインJWT</Keyword> をモバイルアプリに要求し、取得したJWTを使って Server App で認証します。
    </p>
    <p>
      セッションを確立することで、<Keyword>ユーザ識別子</Keyword> や <Keyword><Link to='/ex/datastore'>データストア</Link></Keyword> への接続情報が取得できます。
    </p>
  </div>
)

const GetSession = () => {
  const dispatch = useDispatch()

  const ex = useSelector((state: RootState) => state.ex)

  useEffect(() => {
    dispatch(contextChanged(''))
    if (ex.session === null) {
      dispatch(getSession())
    }
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(getSession())
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="セッションの確立" />
      </div>
      <div className="field">
        <label className="label is-small">ユーザ識別子</label>
        <div className="control is-expanded">
          <input className="input is-small" placeholder="ユーザ識別子" readOnly value={ex.session?.user._id || ''} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">アプリ識別子</label>
        <div className="control is-expanded">
          <input className="input is-small" placeholder="アプリ識別子" readOnly value={ex.session?.app._id || ''} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">データストア接続情報</label>
        <div className="control is-expanded">
          <textarea className="textarea is-small" rows={2} placeholder="データストア接続情報" readOnly value={JSON.stringify(ex.session?.datastore, null, 2)} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
    </div>
  )
}
