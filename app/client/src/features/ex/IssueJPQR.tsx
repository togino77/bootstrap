import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { showJPQR, getSession, contextChanged } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'
import { MPM_GUIDELINE_URL, JPQR_URL } from '../../constants'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <Notice />
      </div>
      <div className="column is-full">
        <IssueJPQR />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>8. 決済用コード</strong>
    </h1>
    <p>
      店舗側アプリでコード決済に対応する場合は、JPQRの <Keyword>統一動的QRコード</Keyword> を生成して顧客に提示します。
    </p>
    <p>
      顧客は対応している任意のコード決済アプリで支払うことができます。
    </p>
  </div>
)

const Notice = () => (
  <article className="message is-warning is-small">
    <div className="message-body">
      あらかじめ <span className="tag is-warning">統一店舗識別コード</span> の取得が必要です。
      詳細は <a href={JPQR_URL}>統一QR「JPQR」普及事業</a> および <a href={MPM_GUIDELINE_URL}>統一動的QRコードの仕様</a> をご確認ください。
    </div>
  </article>
)

interface JPQRForm {
  merchantName: string
  merchantPostalCode: string
  merchantCategoryCode: string
  merchantNameEN: string
  merchantCityEN: string
  merchantAccountInformation: string
}

const IssueJPQR = () => {
  const dispatch = useDispatch()
  const ex = useSelector((state: RootState) => state.ex)
  const initialForm = {
    merchantName: 'ナタデココカフェ',
    merchantPostalCode: '5300005',
    merchantCategoryCode: '5812',
    merchantNameEN: 'NATADECOCO',
    merchantCityEN: 'OSAKA',
    merchantAccountInformation: '0019jp.or.paymentsjapan011300000000000010204000103060000010406000001'
  }

  const [form, setForm] = useState<JPQRForm>(initialForm)

  useEffect(() => {
    dispatch(contextChanged(''))
    if (ex.session === null) {
      dispatch(getSession())
    }
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    const opts = { ...form, transactionAmount: ex.total }
    dispatch(showJPQR(opts))
  }

  const onChangeForm = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    switch (e.target.id) {
      case 'merchantAccountInformation':
        setForm({ ...form, merchantAccountInformation: e.target.value })
        break
    }
  }

  const onClickModalClose = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(contextChanged(''))
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="決済用コードの表示" />
      </div>
      <div className="field">
        <label className="label is-small">統一店舗識別コード</label>
        <div className="control is-expanded">
          <textarea className="textarea is-small" id="merchantAccountInformation" rows={1} value={form.merchantAccountInformation} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">取引金額</label>
        <div className="field has-addons">
          <p className="control">
            <a className="button is-static is-small">¥</a>
          </p>
          <div className="control is-expanded">
            <input className="input is-small" type="number" id="amount" readOnly value={ex.total} />
          </div>
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
      <div className={`modal ${ex.jpqr ? 'is-active' : ''}`}>
        <div className="modal-background"></div>
        <div className="modal-content has-text-centered">
          <div className="subtitle">決済用コード</div>
          <figure className="image is-square">
            <img src={ex.jpqr} />
          </figure>
        </div>
        <button className="modal-close is-large" aria-label="close" onClick={onClickModalClose}></button>
      </div>
    </div>
  )
}
