import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import Publisher from '../../images/publisher.svg'
import Keyword from '../../components/Keyword'
import { REPO_URL } from '../../constants'
import { contextChanged, getSession } from './slice'

const Feature = (props: any) => {
  const dispatch = useDispatch()

  const ex = useSelector((state: RootState) => state.ex)

  useEffect(() => {
    dispatch(contextChanged(''))
    if (ex.session === null) {
      dispatch(getSession())
    }
  }, [])

  return (
    <div className="section">
      <div className="columns">
        <div className="column is-full">
          <Greeting />
        </div>
        <div className="column is-full">
          <AboutThis />
        </div>
        <div className="column is-full has-text-centered">
          <AboutRepo />
        </div>
      </div>
    </div>
  )
}

export default Feature

const Greeting = () => (
  <div className="container">
    <h1 className="title">
      <strong>ようこそ！</strong>
    </h1>
    <figure className="image is-4by3">
      <img src={Publisher} className="" width="100%" alt="man" />
    </figure>
  </div>
)

const AboutThis = () => (
  <div className="container">
    <p>
      右上のメニューからモバイルアプリやプラットフォームと連携するさまざまな機能を試すことができます。
    </p>
    <p>
      アプリコンテンツ制作の <Keyword>ブートストラップ</Keyword> としてご活用ください！
    </p>
  </div>
)

const AboutRepo = () => (
  <div className="container">
    <a href={REPO_URL}>
      <button className="button is-small is-info">
        <span className="icon">
          <i className="fas fa-code"></i>
        </span>
        <span><strong>ソースコードをみる</strong></span>
      </button>
    </a>
  </div>
)