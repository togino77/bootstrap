import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { issueNotification, contextChanged } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <IssueNotification />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>5. プッシュ通知</strong>
    </h1>
    <p>
      アプリを配信しているナタデココスポットのエリア外にいるユーザに、プッシュ通知でお知らせすることができます。
      通知は送信元がアプリ、送信先がユーザとなります。
    </p>
  </div>
)

interface NotificationForm {
  recipient: string
  title: string
  message: string
  url: string
}

const IssueNotification = () => {
  const dispatch = useDispatch()
  const ex = useSelector((state: RootState) => state.ex)
  const initialForm = {
    recipient: ex.session?.user._id,
    title: 'Bootstrap',
    message: 'こんにちは！',
    url: 'https://natade-coco.com'
  }
  const [form, setForm] = useState<NotificationForm>(initialForm)

  useEffect(() => {
    dispatch(contextChanged(''))
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(issueNotification(form.recipient, form.title, form.message, form.url))
  }

  const onChangeForm = (e: React.ChangeEvent<HTMLInputElement>) => {
    switch (e.target.id) {
      case 'recipient':
        setForm({ ...form, recipient: e.target.value })
        break
      case 'title':
        setForm({ ...form, title: e.target.value })
        break
      case 'message':
        setForm({ ...form, message: e.target.value })
        break
      case 'url':
        setForm({ ...form, url: e.target.value })
        break
    }
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="プッシュ通知の送信" />
      </div>
      <div className="field">
        <label className="label is-small">宛先ユーザの識別子</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="recipient" value={form.recipient} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">タイトル</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="title" value={form.title} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">メッセージ</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="message" value={form.message} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">開封時に開くURL</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="url" value={form.url} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
    </div>
  )
}
