import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { addItem, getSession, removeItem, contextChanged } from './slice'
import Ex from '../../components/Ex'
import { Item, CartItem } from '../../services/datastore'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <UseDatastore />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>6. データストア</strong>
    </h1>
    <p>
      データストアはユニット単位で利用でき、同じユニットで配信するアプリ間でデータを共有することができます。
    </p>
    <p>
      サンプルでは商品リストとユーザごとの買い物かごにデータストアを利用しています。
    </p>
  </div>
)

const UseDatastore = () => {
  const dispatch = useDispatch()

  const ex = useSelector((state: RootState) => state.ex)

  useEffect(() => {
    dispatch(contextChanged(''))
    if (ex.session === null) {
      dispatch(getSession())
    }
  }, [])

  const onAddItem = (item_id: string) => {
    dispatch(addItem(ex.cart.id, item_id))
  }

  const onRemoveItem = (item_id: string) => {
    const inCart = ex.cartItems.find((e: any) => e.item_id === item_id)
    if (inCart) {
      dispatch(removeItem(ex.cart.id, inCart.id))
    }
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="買い物かご" />
      </div>
      <div className="field">
        <label className="label is-small">合計金額</label>
        <div className="field has-addons">
          <p className="control">
            <a className="button is-static is-small">¥</a>
          </p>
          <div className="control is-expanded">
            <input className="input is-small" type="number" readOnly value={String(ex.total)} />
          </div>
        </div>
      </div>
      <div className="field">
        <label className="label is-small">商品リスト{ex.isLoading && !ex.items ? "(取得中)" : ""}</label>
        <p className="help is-danger">{ex.error}</p>
        <div className="container">
          {ex.items?.map((item: Item) => {
            return <ShowItem
              key={item.id}
              item={item}
              add={onAddItem}
              remove={onRemoveItem}
              quantity={ex.cartItems?.filter((e: CartItem) => e.item_id === item.id).length}
              isLoading={ex.isLoading}
            />
          })}
        </div>
      </div>
    </div>
  )
}

interface ItemProps {
  item: Item
  add: (item_id: string) => void
  remove: (item_id: string) => void
  quantity: number
  isLoading: boolean
}

const ShowItem = (props: ItemProps) => {
  const onClickAdd = (e: React.MouseEvent<HTMLInputElement>) => {
    props.add(props.item.id)
  }

  const onClickRemove = (e: React.MouseEvent<HTMLInputElement>) => {
    props.remove(props.item.id)
  }

  return (
    <div className="card">
      <div className="card-content">
        <div className="media">
          <div className="media-left">
            <figure className="image is-96x96">
              <img src={props.item.image?.thumbnails[3]?.url} alt="商品イメージ" />
            </figure>
          </div>
          <div className="media-content">
            <p className="title is-4 has-text-grey">{props.item.name}</p>
            <p className="subtitle is-6 has-text-grey">¥ {props.item.price}</p>
          </div>
        </div>
        <div className="content">
          {props.item.description}
        </div>
      </div>
      <footer className="card-footer">
        <div className="card-footer-item">
          <button onClick={onClickRemove} className={`button is-small ${props.isLoading ? "is-loading" : ""}`} style={{ border: "none" }}>
            <span className="icon">
              <i className="fas fa-minus"></i>
            </span>
          </button>
        </div>
        <div className="card-footer-item">
          <button className={`button is-small is-static ${props.isLoading ? "is-loading" : ""}`} style={{ border: "none" }}>
            <span>{props.quantity}</span> <span>点</span>
          </button>
        </div>
        <div className="card-footer-item">
          <button onClick={onClickAdd} className={`button is-small ${props.isLoading ? "is-loading" : ""}`} style={{ border: "none" }}>
            <span className="icon">
              <i className="fas fa-plus"></i>
            </span>
          </button>
        </div>
      </footer>
    </div>
  )
}