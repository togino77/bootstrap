import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { fetchCCInfo, contextChanged } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <Notice />
      </div>
      <div className="column is-full">
        <GetCCInfo />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>3. クレジットカード情報</strong>
    </h1>
    <p>
      Pocket SDKを通してクレジットカード情報を要求することができます。取得できる情報にセキュリティコードは含まれません。
      要求時には <Keyword>認可ダイアログ</Keyword> の表示および <Keyword>生体認証</Keyword> が行われます。
    </p>
  </div>
)

const Notice = () => (
  <article className="message is-warning is-small">
    <div className="message-body">
      クレジットカード情報を取り扱う場合、「クレジットカード情報の非保持化」またはPCI DSS（Payment Card Industry Data Security Standard）への準拠が必要です。
    </div>
  </article>
)

const GetCCInfo = () => {
  const dispatch = useDispatch()

  const ex = useSelector((state: RootState) => state.ex)

  useEffect(() => {
    dispatch(contextChanged(''))
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(fetchCCInfo())
  }

  const maskNumber = (raw: string) => {
    if (!raw) {
      return ''
    }
    return '*'.repeat(raw.length - 4) + raw.substr(-4)
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="カード情報の要求" />
      </div>
      <div className="field">
        <label className="label is-small">カード番号</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" placeholder="カード番号" readOnly value={maskNumber(ex.number) || ""} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">有効期限</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" placeholder="有効期限" readOnly value={ex.exp || ""} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
    </div>
  )
}
