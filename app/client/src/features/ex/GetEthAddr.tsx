import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { fetchEthereumAddress, contextChanged } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'
import { ETHERSCAN_URL } from '../../constants'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <Notice />
      </div>
      <div className="column is-full">
        <GetEthAddr />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>4. イーサリアムアドレス</strong>
    </h1>
    <p>
      ユーザへのインセンティブ付与などを目的として、モバイルアプリにイーサリアムアドレスを要求することができます。
      要求時には <Keyword>認可ダイアログ</Keyword> が表示されます。
    </p>
  </div>
)

const Notice = () => (
  <article className="message is-warning is-small">
    <div className="message-body">
      アドレスは <span className="tag is-warning">メインネット(Mainnet)</span> に属しています。
      トランザクション実行時はユーザに <a href={ETHERSCAN_URL}>etherscan.io</a> でトランザクションの詳細を通知するようにしてください。
    </div>
  </article>
)

const GetEthAddr = () => {
  const dispatch = useDispatch()

  const ex = useSelector((state: RootState) => state.ex)

  useEffect(() => {
    dispatch(contextChanged(''))
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(fetchEthereumAddress())
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="イーサリアムアドレスの要求" />
      </div>
      <div className="field">
        <label className="label is-small">イーサリアムアドレス</label>
        <div className="control is-expanded">
          <textarea className="textarea is-small" rows={1} placeholder="イーサリアムアドレス" readOnly value={ex.ethAddr || ""} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
    </div>
  )
}
