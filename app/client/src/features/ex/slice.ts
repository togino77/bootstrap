import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AppThunk } from "../../app/store"
import * as Pocket from '../../services/pocket'
import * as Server from '../../services/server'
import * as WASM from '../../services/wasm'
import * as Datastore from '../../services/datastore'

export interface State {
  session?: Server.Session | null
  name?: string
  fullName?: string | null
  email?: string | null
  tel?: string | null
  image?: string | null
  number: string
  exp: string
  ethAddr: string
  connect: string | null
  jpqr: string | null
  items: Datastore.Item[] | null
  cart: Datastore.Cart
  cartItems: Datastore.CartItem[] | null
  total: number
  isLoading: boolean
  error: string | null
}

const initialState: State = {
  session: null,
  name: null,
  fullName: null,
  email: null,
  tel: null,
  image: null,
  number: '',
  exp: '',
  ethAddr: '',
  connect: null,
  jpqr: null,
  items: null,
  cart: null,
  cartItems: null,
  total: 0,
  isLoading: false,
  error: null
}

function startLoading(state: State) {
  state.isLoading = true
}

function loadingFailed(state: State, action: PayloadAction<any>) {
  state.isLoading = false
  state.error = action.payload
  state.jpqr = null
  state.connect = null
}

const app = createSlice({
  name: 'app',
  initialState,
  reducers: {
    contextChanged: loadingFailed,
    getSessionStart: startLoading,
    getSessionSuccess(state, { payload }: PayloadAction<Server.Session>) {
      state.session = payload
      state.isLoading = false
      state.error = null
    },
    getSessionFailed: loadingFailed,
    issueNotificationStart: startLoading,
    issueNotificationSuccess(state, { payload }: PayloadAction<any>) {
      state.isLoading = false
      state.error = null
    },
    issueNotificationFailed: loadingFailed,
    getProfileStart: startLoading,
    getProfileSuccess(state, { payload }: PayloadAction<Pocket.Profile>) {
      state.name = payload.name
      state.fullName = payload.fullName
      state.email = payload.email
      state.tel = payload.tel
      state.image = payload.image
      state.isLoading = false
      state.error = null
    },
    getProfileFailure: loadingFailed,
    getCCInfoStart: startLoading,
    getCCInfoSuccess(state, { payload }: PayloadAction<Pocket.CCInfo>) {
      state.number = payload.number
      state.exp = payload.exp
      state.isLoading = false
      state.error = null
    },
    getCCInfoFailure: loadingFailed,
    getEthereumAddressStart: startLoading,
    getEthereumAddressSuccess(state, { payload }: PayloadAction<string>) {
      state.ethAddr = payload
      state.isLoading = false
      state.error = null
    },
    getEthereumAddressFailure: loadingFailed,
    getConnectAppStart: startLoading,
    getConnectAppSuccess(state, { payload }: PayloadAction<string>) {
      state.connect = payload
      state.isLoading = false
      state.error = null
    },
    getConnectAppFailure: loadingFailed,
    getJPQRStart: startLoading,
    getJPQRSuccess(state, { payload }: PayloadAction<string>) {
      state.jpqr = payload
      state.isLoading = false
      state.error = null
    },
    getJPQRFailed: loadingFailed,
    getItemsStart: startLoading,
    getItemsSuccess(state, { payload }: PayloadAction<Datastore.Item[]>) {
      state.items = payload
      state.isLoading = false
      state.error = null
    },
    getItemsFailed: loadingFailed,
    getCartStart: startLoading,
    getCartSuccess(state, { payload }: PayloadAction<Datastore.Cart>) {
      state.cart = payload
      state.isLoading = false
      state.error = null
    },
    getCartFailed: loadingFailed,
    getCartItemsStart: startLoading,
    getCartItemsSuccess(state, { payload }: PayloadAction<Datastore.CartItem[]>) {
      state.cartItems = payload
      let total = 0;
      for (let i = 0; i < state.cartItems?.length; i++) {
        const ci = state.cartItems[i];
        const item = state.items?.find((e: any) => e.id === ci.item_id)
        if (item) {
          total += item.price
        }
      }
      state.total = total
      state.isLoading = false
      state.error = null
    },
    getCartItemsFailed: loadingFailed,
    addItemStart: startLoading,
    addItemSuccess(state, { payload }: PayloadAction<boolean>) {
      state.isLoading = false
      state.error = null
    },
    addItemFailed: loadingFailed,
    removeItemStart: startLoading,
    removeItemSuccess(state, { payload }: PayloadAction<boolean>) {
      state.isLoading = false
      state.error = null
    },
    removeItemFailed: loadingFailed
  }
})

export const {
  contextChanged,
  getSessionStart,
  getSessionSuccess,
  getSessionFailed,
  issueNotificationStart,
  issueNotificationSuccess,
  issueNotificationFailed,
  getProfileStart,
  getProfileSuccess,
  getProfileFailure,
  getCCInfoStart,
  getCCInfoSuccess,
  getCCInfoFailure,
  getEthereumAddressStart,
  getEthereumAddressSuccess,
  getEthereumAddressFailure,
  getConnectAppStart,
  getConnectAppSuccess,
  getConnectAppFailure,
  getJPQRStart,
  getJPQRSuccess,
  getJPQRFailed,
  getItemsStart,
  getItemsSuccess,
  getItemsFailed,
  getCartStart,
  getCartSuccess,
  getCartFailed,
  getCartItemsStart,
  getCartItemsSuccess,
  getCartItemsFailed,
  addItemStart,
  addItemSuccess,
  addItemFailed,
  removeItemStart,
  removeItemSuccess,
  removeItemFailed
} = app.actions

export default app.reducer

export const getSession = (): AppThunk => async dispatch => {
  try {
    dispatch(getSessionStart())
    const jwt = await Pocket.requestSignJWT()
    const result = await Server.getSession(jwt)
    Datastore.newClient(result.datastore)
    dispatch(getSessionSuccess(result))
    dispatch(fetchItems(result.user._id))
  } catch (err) {
    dispatch(getSessionFailed(err.message ? err.message : err))
  }
}

export const issueNotification = (recipient: string, title: string, message: string, url: string): AppThunk => async dispatch => {
  try {
    dispatch(issueNotificationStart())
    const result = await Server.issueNotification(recipient, title, message, url);
    dispatch(issueNotificationSuccess(result))
  } catch (err) {
    dispatch(issueNotificationFailed(err.message))
  }
}

export const fetchProfile = (scope: string[]): AppThunk => async dispatch => {
  try {
    dispatch(getProfileStart())
    const result = await Pocket.getProfile(scope)
    dispatch(getProfileSuccess(result))
  } catch (err) {
    dispatch(getProfileFailure(err))
  }
}

export const fetchCCInfo = (): AppThunk => async dispatch => {
  try {
    dispatch(getCCInfoStart())
    const result = await Pocket.getCCInfo()
    dispatch(getCCInfoSuccess(result))
  } catch (err) {
    dispatch(getCCInfoFailure(err))
  }
}

export const fetchEthereumAddress = (): AppThunk => async dispatch => {
  try {
    dispatch(getEthereumAddressStart())
    const result = await Pocket.getEthereumAddress()
    dispatch(getEthereumAddressSuccess(result))
  } catch (err) {
    dispatch(getEthereumAddressFailure(err))
  }
}

export const showConnection = (target: string, path: string, allows: string[]): AppThunk => async dispatch => {
  try {
    dispatch(getConnectAppStart())
    const result = await Pocket.connectToPayoffApp(target, path, allows)
    dispatch(getConnectAppSuccess(result))
  } catch (err) {
    dispatch(getConnectAppFailure(err))
  }
}

export const showJPQR = (opts: WASM.MPMOpts): AppThunk => async dispatch => {
  try {
    dispatch(getJPQRStart())
    const result = await WASM.getJPQR(opts)
    dispatch(getJPQRSuccess(result))
  } catch (err) {
    dispatch(getJPQRFailed(err))
  }
}

export const fetchItems = (customer_id: string): AppThunk => async dispatch => {
  try {
    dispatch(getItemsStart())
    const result = await Datastore.getItems()
    dispatch(getItemsSuccess(result))
    dispatch(fetchCart(customer_id))
  } catch (err) {
    dispatch(getItemsFailed(err))
  }
}

export const fetchCart = (customer_id: string): AppThunk => async dispatch => {
  try {
    dispatch(getCartStart())
    const cart = await Datastore.getCart(customer_id)
    dispatch(getCartSuccess(cart))
    dispatch(fetchCartItems(cart.id))
  } catch (err) {
    dispatch(getCartFailed(err))
  }
}
export const fetchCartItems = (cart_id: string): AppThunk => async dispatch => {
  try {
    dispatch(getCartItemsStart())
    const items = await Datastore.getCartItems(cart_id)
    dispatch(getCartItemsSuccess(items))
  } catch (err) {
    dispatch(getCartItemsFailed(err))
  }
}

export const addItem = (cart_id: string, item_id: string): AppThunk => async dispatch => {
  try {
    dispatch(addItemStart())
    const cartItem = await Datastore.addItemTo(cart_id, item_id)
    dispatch(addItemSuccess(true))
    dispatch(fetchCartItems(cart_id))
  } catch (err) {
    dispatch(addItemFailed(err))
  }
}

export const removeItem = (cart_id: string, cart_item_id: string): AppThunk => async dispatch => {
  try {
    dispatch(removeItemStart())
    const result = await Datastore.removeItem(cart_item_id)
    dispatch(removeItemSuccess(result))
    dispatch(fetchCartItems(cart_id))
  } catch (err) {
    dispatch(removeItemFailed(err))
  }
}